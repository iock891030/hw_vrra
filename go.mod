module VRRA

go 1.17

replace sevice => ./sevice/

require (
	github.com/go-sql-driver/mysql v1.6.0
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f
	sevice v0.0.0-00010101000000-000000000000
)

require (
	github.com/alexcesaro/mail v0.0.0-20141015155039-29068ce49a17 // indirect
	github.com/kr/pretty v0.2.1 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
)
