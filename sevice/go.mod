module sevice

go 1.17

require (
	github.com/alexcesaro/mail v0.0.0-20141015155039-29068ce49a17
	github.com/arran4/golang-ical v0.0.0-20211212012649-32b67e209c4f
	github.com/go-sql-driver/mysql v1.6.0
	github.com/satori/go.uuid v1.2.0
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
