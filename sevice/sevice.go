package sevice

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"

	// "os/user"
	"strconv"
	"time"

	"github.com/alexcesaro/mail/gomail"
	_ "github.com/go-sql-driver/mysql"
	_uuid "github.com/satori/go.uuid"
	"golang.org/x/net/websocket"
)

func connect2database() *sql.DB {
	// set the database route and acc here
	db, err := sql.Open("mysql", "root:Password@tcp(127.0.0.1:3306)/VRRAdatabase")
	if err != nil {
		fmt.Println("connect2database Err: ", err)
	}
	return db
}
func Sendtoclient(data map[string]string, ws *websocket.Conn) error {
	rep, err := json.Marshal(data)
	if err != nil {
		fmt.Println("err in ECHO_LOGIN_JSON_MAR :", err)
	}
	if err = websocket.Message.Send(ws, string(rep)); err != nil {
		fmt.Println("Can't send to " + ws.Request().RemoteAddr)
		return err
	}
	fmt.Println("send to client" + string(rep))
	return nil
}
func ResponRoomEvent2client(data responRoomEvent, ws *websocket.Conn) error {
	rep, err := json.Marshal(data)
	if err != nil {
		fmt.Println("err in ECHO_LOGIN_JSON_MAR :", err)
	}
	if err = websocket.Message.Send(ws, string(rep)); err != nil {
		fmt.Println("Can't send to " + ws.Request().RemoteAddr)
		return err
	}
	fmt.Println("send to client" + string(rep))
	return nil
}
func UserLogin(acc string, password string) (state string, userMail string, user_first_name string, user_second_name string) {

	db := connect2database()
	defer db.Close()
	result := db.QueryRow("SELECT user_password FROM `usertable` WHERE user_account = ?", acc)
	var pass string
	err := result.Scan(&pass)
	if err != nil {
		fmt.Println("userlogin Err: ", err)
		state = "2"
		userMail = ""
		return
	}
	if password != pass {
		fmt.Println("show password:" + password)
		fmt.Println("show pass:" + pass)
		state = "1"
		userMail = ""
		return
	}
	reMail := db.QueryRow("SELECT user_mail, user_first_name, user_second_name FROM `usertable` WHERE user_account = ?", acc)
	err = reMail.Scan(&userMail, &user_first_name, &user_second_name)
	if err != nil {
		fmt.Println("userlogin Err: ", err)
		state = "2"
		userMail = ""
		return
	}
	state = "0"
	return
}
func EditUserInfo(acc string, data map[string]string) string {
	state := checkUserEditdata(data)
	if state == "0" {
		db := connect2database()
		defer db.Close()
		if data["Password"] == "" {
			_, err := db.Exec("UPDATE usertable SET user_first_name = ?, user_second_name = ? WHERE user_account = ?;", data["FirstName"], data["SecondName"], acc)
			if err != nil {
				fmt.Println("err in useredit :", err)
				state = "1450"
			}
		} else {
			_, err := db.Exec("UPDATE usertable SET user_first_name = ?, user_second_name = ?, user_password = ? WHERE user_account = ?;", data["FirstName"], data["SecondName"], data["Password"], acc)
			if err != nil {
				fmt.Println("err in useredit :", err)
				state = "1450"
			}
		}
	}
	return state
}
func checkUserEditdata(data map[string]string) string {
	if len(data["FirstName"]) > 16 {
		return "5"
	}
	if len(data["SecondName"]) > 24 {
		return "5"
	}
	if len(data["Passward"]) > 32 {
		return "5"
	}
	if len(data["FirstName"]) == 0 && len(data["SecondName"]) == 0 {
		return "2"
	}
	if len(data["Password"]) == 0 {
		return "0"
	}
	return "0"
}
func checkRegisterdata(data map[string]string) string {
	if len(data["UserMail"]) > 64 {
		return "5"
	}
	if len(data["Account"]) > 24 {
		return "5"
	}
	if len(data["FirstName"]) > 16 {
		return "5"
	}
	if len(data["SecondName"]) > 24 {
		return "5"
	}
	if len(data["Passward"]) > 32 {
		return "5"
	}
	if len(data["FirstName"]) == 0 && len(data["SecondName"]) == 0 {
		return "2"
	}
	if len(data["Password"]) == 0 {
		return "3"
	}
	cor := strings.Contains(data["UserMail"], "@")
	if !cor {
		return "4"
	}
	return "0"
}
func UserRegister(data map[string]string) string {
	state := checkRegisterdata(data)
	if state == "0" {
		db := connect2database()
		defer db.Close()
		ins := "INSERT INTO `usertable` VALUES('" + data["Account"] + "','" + data["UserMail"] + "','"
		ins = ins + data["FirstName"] + "','" + data["SecondName"] + "','" + data["Password"] + "');"
		_, err := db.Exec(ins)
		if err != nil {
			fmt.Println("err in userRegister:", err)
			return "1"
		}
		return "0"
	}
	return state
}

type roomEvent struct {
	uuid            string
	room_name       string
	rent_By         string
	invites         string
	theme           string
	rent_date       string
	rent_Start_time string
	rent_End_time   string
	agenda          string
	note            string
}
type responRoomEvent struct {
	Type    string
	SetDate string
	Data    []map[string]string
}

func easyAtoi(s string) int {
	n, err := strconv.Atoi(s)
	if err != nil {
		fmt.Println("ERR in ezAtoi", err)
	}
	return n
}
func checkRentData(data map[string]string) string {
	if data["Room"] != "RoomA1" && data["Room"] != "RoomA2" && data["Room"] != "RoomA3" && data["Room"] != "RoomB1" && data["Room"] != "RoomB2" && data["Room"] != "RoomC1" && data["Room"] != "RoomC2" {
		return "2"
	}
	if da := data["Date"]; len(da) == 0 {
		return "3"
	}
	if ti := data["TimeStart"]; len(ti) == 0 {
		return "3"
	}
	if ti := data["TimeEnd"]; len(ti) == 0 {
		return "3"
	}

	roomEs := FetchRoomByDate(data["Date"], "master")
	for _, k := range roomEs.Data {
		if k["Room"] == data["Room"] {
			if easyAtoi(data["TimeStart"]) <= easyAtoi(k["TimeEnd"]) && easyAtoi(data["TimeStart"]) >= easyAtoi(k["TimeStart"]) {
				if data["RUID"] != k["RUID"] {
					return "1"
				}
			}
			if easyAtoi(data["TimeEnd"]) <= easyAtoi(k["TimeEnd"]) && easyAtoi(data["TimeEnd"]) >= easyAtoi(k["TimeStart"]) {
				if data["RUID"] != k["RUID"] {
					return "1"
				}
			}
		}
	}

	return "0"
}
func compairEditRoomData(fromdb roomEvent, data map[string]string) (diff string, EdgeGuys []string, newGuy []string) {
	diff = ""
	if fromdb.theme != data["Theme"] {
		diff += "主旨: " + fromdb.theme + " => " + data["Theme"] + "\n"
	}
	if fromdb.room_name != data["Room"] {
		diff += "地點: " + fromdb.room_name + " => " + data["Room"] + "\n"
	}
	if fromdb.rent_date != data["Date"] {
		diff += "日期: " + fromdb.rent_date + " => " + data["Date"] + "\n"
	}
	if fromdb.rent_Start_time != data["TimeStart"] || fromdb.rent_End_time != data["TimeEnd"] {
		diff += "時間: " + fromdb.rent_Start_time + "~" + fromdb.rent_End_time + " => " + data["TimeStart"] + "~" + data["TimeEnd"] + "\n"
	}
	if fromdb.agenda != data["Agenda"] {
		diff += "更新議程:\n " + data["Agenda"] + "\n\n"
	}
	if fromdb.invites != data["Invites"] {
		invitesFromdb := strings.Split(fromdb.invites, ";")
		Editinvites := strings.Split(data["Invites"], ";")
		comp := make(map[string]int)
		var edgeGuys []string
		for _, invite := range invitesFromdb {
			comp[invite] = 2
		}
		for _, invite := range Editinvites {
			comp[invite]++
		}
		for dif, num := range comp {
			if num != 3 {
				if num == 2 {
					edgeGuys = append(edgeGuys, dif)
				} else {
					newGuy = append(newGuy, dif)
				}
			}
		}
		EdgeGuys = edgeGuys
	}
	return
}
func EditRoomByRoomUUID(acc string, data map[string]string) string {
	state := checkRentData(data)
	if state == "0" {
		db := connect2database()
		defer db.Close()
		var aRoom roomEvent
		row := db.QueryRow("SELECT * FROM `roomtable` WHERE uuid = ?", data["RUID"])
		err := row.Scan(&aRoom.uuid, &aRoom.room_name, &aRoom.rent_By, &aRoom.invites, &aRoom.theme, &aRoom.rent_date, &aRoom.rent_Start_time, &aRoom.rent_End_time, &aRoom.agenda, &aRoom.note)
		if err != nil {
			fmt.Println("err in EditRoomByRoomUUID: ", err)
			return "1"
		}
		_, err = db.Exec("UPDATE roomtable SET  room_name =?,  invites = ?, theme = ?, rent_date = ?, rent_Start_time = ?, rent_End_time = ?, agenda = ?, note = ? WHERE uuid = ?;", data["Room"], data["Invites"], data["Theme"], data["Date"], data["TimeStart"], data["TimeEnd"], data["Agenda"], data["Note"], data["RUID"])
		if err != nil {
			fmt.Println("err in EditRoomByRoomUUID :", err)
			state = "1450"
		}
		diff, EdgeGuy, newGuy := compairEditRoomData(aRoom, data)
		if len(EdgeGuy) != 0 {
			msg := gomail.NewMessage()
			SetAttendee(EdgeGuy, msg)
			SetSubject(aRoom.rent_By+" 取消了 "+aRoom.rent_date+" "+aRoom.rent_Start_time+"~"+aRoom.rent_End_time+"的會議", msg)
			SetCancelBody("text", aRoom, msg)
			Send(msg)
		}
		if len(newGuy) != 0 {
			msg := gomail.NewMessage()
			SetAttendee(newGuy, msg)
			SetSubject(acc+" 邀請您參與會議", msg)
			SetInviteBody("text", data, msg)
			Send(msg)
		}
		if diff != "" {
			msg := gomail.NewMessage()
			invites := strings.Split(data["Invites"], ";")
			SetAttendee(invites, msg)
			SetSubject(aRoom.rent_By+" 更動了會議", msg)
			fmt.Println(diff)
			SetEditRoomBody("text", acc, diff, msg)
			Send(msg)
		}
	}
	return state
}
func CancelRoom(acc string, RUID string) string {
	var aRoom roomEvent
	db := connect2database()
	defer db.Close()
	row := db.QueryRow("SELECT * FROM `roomtable` WHERE uuid = ?", RUID)
	err := row.Scan(&aRoom.uuid, &aRoom.room_name, &aRoom.rent_By, &aRoom.invites, &aRoom.theme, &aRoom.rent_date, &aRoom.rent_Start_time, &aRoom.rent_End_time, &aRoom.agenda, &aRoom.note)
	if err != nil {
		fmt.Println("err in cancelRoom: ", err)
		return "1"
	}

	if aRoom.rent_By == acc {
		_, err := db.Exec("DELETE FROM `roomtable` WHERE uuid = ?;", RUID)
		if err != nil {
			fmt.Println("err in cancelRoom: ", err)
			return "1"
		}
		msg := gomail.NewMessage()
		invites := strings.Split(aRoom.invites, ";")
		fmt.Println(invites)
		if len(invites) > 0 {
			SetAttendee(invites, msg)
			SetSubject(aRoom.rent_By+" 取消了 "+aRoom.rent_date+" "+aRoom.rent_Start_time+"~"+aRoom.rent_End_time+"的會議", msg)
			SetCancelBody("text", aRoom, msg)
			Send(msg)
		}
	}

	return "0"
}

func FetchRoomByDate(date string, mod string) responRoomEvent {
	result := new(responRoomEvent)
	result.Type = "FETCHRBD"
	result.SetDate = date
	fmt.Println("date in RBD", date)
	db := connect2database()
	defer db.Close()
	rows, err := db.Query("SELECT uuid, room_name, rent_By,theme,rent_Start_time, rent_End_time FROM `roomtable` WHERE rent_date = ?", date)
	if err != nil {
		fmt.Println("err in fetchRoomByDate:", err)
	}
	for rows.Next() {
		var aRoom = new(roomEvent)
		err := rows.Scan(&aRoom.uuid, &aRoom.room_name, &aRoom.rent_By, &aRoom.theme, &aRoom.rent_Start_time, &aRoom.rent_End_time)
		if err != nil {
			fmt.Println("err in fetchRoomByDate:", err)
		} else {
			fmt.Println(aRoom)
			mapAroom := make(map[string]string)
			mapAroom["Room"] = aRoom.room_name
			mapAroom["ChairPerson"] = aRoom.rent_By
			mapAroom["Theme"] = aRoom.theme
			if mod == "master" {
				mapAroom["RUID"] = aRoom.uuid
			}
			// mapAroom["Date"] = aRoom.rent_date
			mapAroom["TimeStart"] = aRoom.rent_Start_time
			mapAroom["TimeEnd"] = aRoom.rent_End_time
			// mapAroom["aganda"] = aRoom.agenda
			// mapAroom["note"] = aRoom.note
			result.Data = append(result.Data, mapAroom)
		}
	}
	fmt.Println(result)
	defer rows.Close()
	return *result
}
func FetchRoomByActivity(dateAfter string, account string, regMail string) responRoomEvent {
	fmt.Println("acc in frac at sevice:", account, regMail)
	result := new(responRoomEvent)
	result.Type = "FETCHRAC"
	result.SetDate = dateAfter
	db := connect2database()
	defer db.Close()
	rows, err := db.Query("SELECT *FROM `roomtable` WHERE rent_date >= ?", dateAfter)
	if err != nil {
		fmt.Println("err in fetchRoomByActivie:", err)
	}
	for rows.Next() {
		var aRoom = new(roomEvent)
		err := rows.Scan(&aRoom.uuid, &aRoom.room_name, &aRoom.rent_By, &aRoom.invites, &aRoom.theme, &aRoom.rent_date, &aRoom.rent_Start_time, &aRoom.rent_End_time, &aRoom.agenda, &aRoom.note)
		if err != nil {
			fmt.Println("err in fetchRoomByActivie:", err)
		} else {
			fmt.Println(aRoom)

			if aRoom.rent_By == regMail {
				mapAroom := make(map[string]string)
				mapAroom["RUID"] = aRoom.uuid
				mapAroom["Room"] = aRoom.room_name
				mapAroom["ChairPerson"] = aRoom.rent_By
				mapAroom["Theme"] = aRoom.theme
				mapAroom["Invites"] = aRoom.invites
				mapAroom["Date"] = aRoom.rent_date
				mapAroom["TimeStart"] = aRoom.rent_Start_time
				mapAroom["TimeEnd"] = aRoom.rent_End_time
				mapAroom["Agenda"] = aRoom.agenda
				mapAroom["Note"] = aRoom.note
				result.Data = append(result.Data, mapAroom)
			} else {
				mails := strings.Split(aRoom.invites, ";")
				for _, mail := range mails {
					if mail == regMail && mail != "" {
						mapAroom := make(map[string]string)
						mapAroom["Room"] = aRoom.room_name
						mapAroom["ChairPerson"] = aRoom.rent_By
						mapAroom["Theme"] = aRoom.theme
						mapAroom["Invites"] = aRoom.invites
						mapAroom["Date"] = aRoom.rent_date
						mapAroom["TimeStart"] = aRoom.rent_Start_time
						mapAroom["TimeEnd"] = aRoom.rent_End_time
						mapAroom["Agenda"] = aRoom.agenda
						// mapAroom["Note"] = aRoom.note
						result.Data = append(result.Data, mapAroom)
					}
				}
			}

		}
	}
	fmt.Println(result)
	defer rows.Close()
	return *result
}
func FetchRoomByDateInterval(dateBegin string, dateEnd string) responRoomEvent {
	result := new(responRoomEvent)
	result.Type = "FETCHRDI"
	result.SetDate = dateBegin + "~" + dateEnd
	db := connect2database()
	defer db.Close()
	rows, err := db.Query("SELECT room_name, rent_By, rent_date, theme, rent_Start_time, rent_End_time FROM `roomtable` WHERE rent_date >= ? AND rent_date <= ?", dateBegin, dateEnd)
	if err != nil {
		fmt.Println("err in fetchRoomByDate:", err)
	}
	for rows.Next() {
		var aRoom = new(roomEvent)
		err := rows.Scan(&aRoom.room_name, &aRoom.rent_By, &aRoom.rent_date, &aRoom.theme, &aRoom.rent_Start_time, &aRoom.rent_End_time)
		if err != nil {
			fmt.Println("err in fetchRoomByDate:", err)
		} else {
			fmt.Println(aRoom)
			mapAroom := make(map[string]string)
			mapAroom["Room"] = aRoom.room_name
			mapAroom["ChairPerson"] = aRoom.rent_By
			mapAroom["Date"] = aRoom.rent_date
			mapAroom["Theme"] = aRoom.theme
			mapAroom["TimeStart"] = aRoom.rent_Start_time
			mapAroom["TimeEnd"] = aRoom.rent_End_time
			// mapAroom["aganda"] = aRoom.agenda
			// mapAroom["note"] = aRoom.note
			result.Data = append(result.Data, mapAroom)
		}
	}
	fmt.Println(result)
	defer rows.Close()
	return *result
}

func RentRoom(data map[string]string) string {
	state := checkRentData(data)
	if state == "0" {
		db := connect2database()
		defer db.Close()
		RoomUuid := _uuid.NewV4()
		_, err := db.Exec("insert into `roomtable` values(?,?,?,?,?,?,?,?,?,?)", RoomUuid.String(), data["Room"], data["User"], data["Invites"], data["Theme"], data["Date"], data["TimeStart"], data["TimeEnd"], data["Agenda"], data["Note"])
		if err != nil {
			fmt.Println("err in rentRoom:", err)
			return "6"
		}
		msg := gomail.NewMessage()
		invites := strings.Split(data["Invites"], ";")
		if len(invites) > 0 {
			SetAttendee(invites, msg)
			SetSubject(data["user"]+" 邀請您參與會議", msg)
			SetInviteBody("text", data, msg)
			Send(msg)
		}
		return "0"
	}

	return state
}
func SetAttendee(invites []string, msg *gomail.Message) {
	for _, a := range invites {
		if strings.Contains(a, "@") {
			fmt.Println("a:", a)
			msg.AddHeader("To", a)
		}
	}
}
func SetSubject(Sub string, msg *gomail.Message) {
	msg.SetHeader("Subject", Sub)
}
func SetInviteBody(contentType string, data map[string]string, msg *gomail.Message) {
	sendBody := data["User"] + " 邀請您參與會議\n"
	sendBody += "日期: " + data["Date"] + "\n"
	sendBody += "時間: " + data["TimeStart"] + "~" + data["TimeEnd"] + "\n"
	sendBody += "主旨: " + data["Theme"] + "\n"
	sendBody += "詳情請上VRRA系統了解"
	msg.SetBody(contentType, sendBody)
}
func SetCancelBody(contentType string, data roomEvent, msg *gomail.Message) {
	sendBody := data.rent_By + "於" + time.Now().String() + "\n" + " 取消了下述會議\n"
	sendBody += "主旨: " + data.theme + "\n"
	sendBody += "日期: " + data.rent_date + "\n"
	sendBody += "時間: " + data.rent_Start_time + "~" + data.rent_End_time + "\n"
	sendBody += "議程: " + data.agenda + "\n\n"

	sendBody += "屆時不需到場，造成您的不便請見諒"
	msg.SetBody(contentType, sendBody)
}
func SetEditRoomBody(contentType string, chair string, diff string, msg *gomail.Message) {
	sendBody := chair + " 更動了會議\n"
	sendBody += "\n" + diff + "\n"
	sendBody += "詳情請上VRRA系統了解"
	msg.SetBody(contentType, sendBody)
}
func SetRemindBody(contentType string, aRoom roomEvent, msg *gomail.Message) {
	sendBody := "提醒您記得參與會議\n"
	sendBody += "日期: " + aRoom.rent_date + "\n"
	sendBody += "時間: " + aRoom.rent_Start_time + "~" + aRoom.rent_End_time + "\n"
	sendBody += "地點: " + aRoom.room_name + "\n"
	sendBody += "主旨: " + aRoom.theme + "\n"
	sendBody += "詳情請上VRRA系統了解"
	msg.SetBody(contentType, sendBody)
}
func Send(msg *gomail.Message) {
	msg.SetAddressHeader("From", "3thingcool@gmail.com", "VRRAnoReply")
	m := gomail.NewMailer("smtp.gmail.com", "3thingcool@gmail.com", "rbargvdovvcyrezq", 587)
	if err := m.Send(msg); err != nil {
		fmt.Println("err in mailSender:", err)
	}
}
func Reminder(time string, dateAfter string, hasbeenReminded map[string]bool) error {

	db := connect2database()
	defer db.Close()
	rows, err := db.Query("SELECT *FROM `roomtable` WHERE rent_date = ? ", dateAfter)
	if err != nil {
		fmt.Println("err in fetchRoomByActivie:", err)
	}
	for rows.Next() {
		var aRoom = new(roomEvent)
		err := rows.Scan(&aRoom.uuid, &aRoom.room_name, &aRoom.rent_By, &aRoom.invites, &aRoom.theme, &aRoom.rent_date, &aRoom.rent_Start_time, &aRoom.rent_End_time, &aRoom.agenda, &aRoom.note)
		if err != nil {
			fmt.Println("err in fetchRoomByActivie:", err)
		} else {
			fmt.Println(aRoom)
			idf := easyAtoi(aRoom.rent_Start_time) - easyAtoi(time)
			if !hasbeenReminded[aRoom.uuid] {
				if idf < 100 {
					msg := gomail.NewMessage()
					invites := strings.Split(aRoom.invites, ";")
					if len(invites) > 0 {
						SetAttendee(invites, msg)
						SetSubject("記得參與會議", msg)
						SetRemindBody("text", *aRoom, msg)
						Send(msg)
					}
					hasbeenReminded[aRoom.uuid] = true
					fmt.Println("send reminder to :", aRoom)
					fmt.Println("send reminder to :", hasbeenReminded[aRoom.uuid])
				}
			}
		}
	}
	return nil
}
