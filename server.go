package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	// "os/user"
	"sevice"

	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/net/websocket"
)

func Echo(ws *websocket.Conn) {
	var err error
	login := false
	userAcc := ""
	userMail := ""
	fmt.Println("connected from: " + ws.Request().RemoteAddr)
	for {
		var reply string

		if err = websocket.Message.Receive(ws, &reply); err != nil {
			fmt.Println("Can't receive to " + ws.Request().RemoteAddr)
			break
		}

		fmt.Println("Received back from client: " + reply)

		reqJson := map[string]string{}

		if err := json.Unmarshal([]byte(reply), &reqJson); err != nil {
			fmt.Println("unknow msg from "+ws.Request().RemoteAddr, err)
		}
		if msgType, ok := reqJson["Type"]; ok {
			if msgType == "LOGIN" {
				if login {
					respMap := make(map[string]string)
					respMap["Type"] = "LOGIN"
					respMap["State"] = "3"
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}

				} else {
					userAcc = reqJson["User"]
					userPass := reqJson["Password"]
					lg := ""
					first_name := ""
					second_name := ""
					lg, userMail, first_name, second_name = sevice.UserLogin(userAcc, userPass)
					if lg == "0" {
						login = true
					}
					fmt.Println(lg, userMail, userAcc)
					respMap := make(map[string]string)
					respMap["Type"] = "LOGIN"
					respMap["State"] = lg
					respMap["UserMail"] = userMail
					respMap["FirstName"] = first_name
					respMap["SecondName"] = second_name
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				}
			}
			if msgType == "REGISTER" {
				state := sevice.UserRegister(reqJson)
				respMap := make(map[string]string)
				respMap["Type"] = "REGISTER"
				respMap["State"] = state
				if err := sevice.Sendtoclient(respMap, ws); err != nil {
					break
				}
			}
			if msgType == "USEREDIT" {
				if login {
					state := sevice.EditUserInfo(userAcc, reqJson)
					respMap := make(map[string]string)
					respMap["Type"] = "USEREDIT"
					respMap["State"] = state
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				} else {
					respMap := make(map[string]string)
					respMap["Type"] = "USEREDIT"
					respMap["State"] = "87"
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				}
			}

			if msgType == "RENTROOM" {
				if login {
					reqJson["User"] = userMail
					state := sevice.RentRoom(reqJson)
					respMap := make(map[string]string)
					respMap["Type"] = "RENTROOM"
					respMap["State"] = state
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				} else {
					respMap := make(map[string]string)
					respMap["Type"] = "RENTROOM"
					respMap["State"] = "5"
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}

				}
			}
			if msgType == "EDITRBUID" {
				if login {
					state := sevice.EditRoomByRoomUUID(userAcc, reqJson)
					respMap := make(map[string]string)
					respMap["Type"] = "EDITRBUID"
					respMap["State"] = state
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				} else {
					respMap := make(map[string]string)
					respMap["Type"] = "EDITRBUID"
					respMap["State"] = "87"
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				}
			}
			if msgType == "CANCELRBUID" {
				if login {
					state := sevice.CancelRoom(userMail, reqJson["RUID"])
					respMap := make(map[string]string)
					respMap["Type"] = "CANCELRBUID"
					respMap["State"] = state
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				} else {
					respMap := make(map[string]string)
					respMap["Type"] = "CANCELRBUID"
					respMap["State"] = "87"
					if err := sevice.Sendtoclient(respMap, ws); err != nil {
						break
					}
				}
			}
			if msgType == "FETCHRBD" {
				res := sevice.FetchRoomByDate(reqJson["Date"], "")

				if err := sevice.ResponRoomEvent2client(res, ws); err != nil {
					break
				}
			}
			if msgType == "FETCHRAC" {
				res := sevice.FetchRoomByActivity(reqJson["DateAfter"], userAcc, userMail)
				// fmt.Println("res in server fetchrac:", res)
				if err := sevice.ResponRoomEvent2client(res, ws); err != nil {
					break
				}
			}
			if msgType == "FETCHRDI" {
				res := sevice.FetchRoomByDateInterval(reqJson["DateBegin"], reqJson["DateEnd"])
				if err := sevice.ResponRoomEvent2client(res, ws); err != nil {
					break
				}
			}

		}
	}
}
func beginReminder() (theDate string, theTime string) {
	setTime := time.Now()
	year := setTime.Year()     //年
	month := setTime.Month()   //月
	day := setTime.Day()       //日
	hour := setTime.Hour()     //小时
	minute := setTime.Minute() //分钟

	theDate = strconv.Itoa(year) + "-" + strconv.Itoa(int(month)) + "-" + strconv.Itoa(day)
	theTime = strconv.Itoa(hour) + strconv.Itoa(minute)
	return
}
func time2remind() {
	D := time.Duration(time.Minute * 20)
	T := time.NewTicker(D)
	var hasbeenReminded = map[string]bool{}
	theDate, theTime := beginReminder()
	go func() {
		err := sevice.Reminder(theTime, theDate, hasbeenReminded)
		if err != nil {
			fmt.Println("err in time2remider:", err)
		}
	}()
	for range T.C {

		theDate, theTime := beginReminder()
		go func() {
			err := sevice.Reminder(theTime, theDate, hasbeenReminded)
			if err != nil {
				fmt.Println("err in time2remider:", err)
			}
		}()
	}
}
func init() {

}
func main() {

	go time2remind()

	fs := http.FileServer(http.Dir("./VRRA/dist"))
	// fs := http.FileServer(http.Dir("./webRoot"))
	http.Handle("/", http.StripPrefix("/", fs))
	//http.Handle("/", fs)
	http.Handle("/ws", websocket.Handler(Echo))

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
